package com.maathu.hilttest.api

import com.maathu.hilttest.models.Hotel
import com.maathu.hilttest.util.Constants
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers

interface HotelInterface {



    @Headers("Content-Type:application/json")
    @GET(Constants.HOTEL_END_POINT)
    suspend fun getHotelData():Response<Hotel>
}