package com.maathu.hilttest.viewModels

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.maathu.hilttest.models.Hotel
import com.maathu.hilttest.repositories.HotelRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HotelsViewModel
@Inject constructor(
    private val hotelRepository: HotelRepository
): ViewModel(){

    private val hotelLiveData = MutableLiveData<Hotel>()
    init {
        getAllHotelData()
    }

    val hotelResponse : LiveData<Hotel> get() = hotelLiveData

    private fun getAllHotelData() = viewModelScope.launch{
        hotelRepository.getAllHotelData().let { response ->
            if ( response.isSuccessful){
                hotelLiveData.postValue(response.body())
            }else {
                Log.e("Error",response.errorBody().toString())
            }
        }

    }
}