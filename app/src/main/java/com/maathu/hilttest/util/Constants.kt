package com.maathu.hilttest.util

object Constants {
    const val BASE_URL = "https://dl.dropboxusercontent.com/s/6nt7fkdt7ck0lue/"
    const val HOTEL_END_POINT: String = "hotels"
}