package com.maathu.hilttest.models


import com.google.gson.annotations.SerializedName

data class Image(
    @SerializedName("small")
    val small: String,
    @SerializedName("medium")
    val medium: String,
    @SerializedName("large")
    val large: String
)