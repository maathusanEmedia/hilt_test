package com.maathu.hilttest.models


import com.google.gson.annotations.SerializedName

data class Hotel(
    @SerializedName("status")
    val status: Int,
    @SerializedName("data")
    val `data`: List<Data>
)