package com.maathu.hilttest.repositories

import com.maathu.hilttest.api.HotelInterface
import javax.inject.Inject

class HotelRepository
    @Inject constructor(private val api: HotelInterface){
        suspend fun getAllHotelData() = api.getHotelData()

    }