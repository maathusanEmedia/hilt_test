package com.maathu.hilttest.main.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.maathu.hilttest.R
import com.maathu.hilttest.databinding.ActivityMainBinding
import com.maathu.hilttest.main.adapter.HotelItemAdapter
import com.maathu.hilttest.viewModels.HotelsViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var hotelItemAdapter: HotelItemAdapter
    private val viewModel : HotelsViewModel by viewModels()
    private lateinit var recyclerView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        setUpView()
    }


    private fun setUpView(){
        recyclerView = findViewById(R.id.rv_hotel_list)
        hotelItemAdapter = HotelItemAdapter(listOf())

        recyclerView.adapter = hotelItemAdapter
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        viewModel.hotelResponse.observe(this, { response ->
            hotelItemAdapter.hotelList = response.data
            hotelItemAdapter.notifyDataSetChanged()
        })

    }
}