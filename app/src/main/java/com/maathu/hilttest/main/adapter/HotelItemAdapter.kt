package com.maathu.hilttest.main.adapter

import com.maathu.hilttest.R
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.maathu.hilttest.models.Data


class HotelItemAdapter(
    var hotelList: List<Data>
): RecyclerView.Adapter<HotelItemAdapter.ViewHolder>() {
   inner class ViewHolder(itemView: View)
       :RecyclerView.ViewHolder(itemView){
       val title:TextView = itemView.findViewById(R.id.tv_hotel_title)
       val address:TextView = itemView.findViewById(R.id.tv_hotel_address)
           fun onBind(position: Int){

               val hotel = hotelList[position]

               title.text = hotel.title
               address.text = hotel.address

           }

       }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.hotel_adapter,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(position)
    }

    override fun getItemCount(): Int = hotelList.size
}